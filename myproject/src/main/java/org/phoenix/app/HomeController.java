package org.phoenix.app;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.annotation.Resource;

import org.phoenix.app.dto.CountryDTO;
import org.phoenix.app.model.Drzava;
import org.phoenix.app.service.DrzavaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Resource(name="drzavaService")
	DrzavaService drzavaService;
	
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	
	@RequestMapping(value = "/d", method = RequestMethod.GET)
	public String findAllCountries(Locale locale, Model model) {				
		model.addAttribute("countryDTO", new CountryDTO());	
		model.addAttribute("Lst", drzavaService.findAllCountries() );		
		return "drzavaList";
	}
	
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String addFormCountry(Locale locale, Model model,CountryDTO countryDTO) {					
		model.addAttribute("countryDTO", new CountryDTO());		
		return "addCountry";
	}	
	
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addCountry(Locale locale, Model model,CountryDTO countryDTO) {		
		drzavaService.addCountry(countryDTO);		
		model.addAttribute("Lst", drzavaService.findAllCountries() );		
		return "drzavaList";
	}
		
	@RequestMapping(value = "/remove", method = RequestMethod.POST)
	public @ResponseBody int removeCountry(Locale locale, Model model,int id) {
		
		int rowCount = drzavaService.deleteCountry(id);				
		return rowCount;
	}
	
	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public @ResponseBody Drzava editCountry(Locale locale, Model model, @RequestBody CountryDTO countryDTO) {
		return drzavaService.editCountry(countryDTO);	
	}
	
	
}
