package org.phoenix.app.dao;
// Generated Feb 24, 2016 8:03:36 PM by Hibernate Tools 4.3.1.Final

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.phoenix.app.model.Drzava;
import org.springframework.stereotype.Repository;

/**
 * Home object for domain model class Drzava.
 * @see org.phoenix.app.model.Drzava
 * @author Hibernate Tools
 */
@Repository
public class DrzavaHome implements IDrzava {

	private static final Log log = LogFactory.getLog(DrzavaHome.class);

	@PersistenceContext
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.phoenix.app.modelx.IDrzava#persist(org.phoenix.app.modelx.Drzava)
	 */
	@Override
	public void persist(Drzava transientInstance) {
		log.debug("persisting Drzava instance");
		try {
			System.out.println("AXAXAXAXAXAX");
			entityManager.persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.modelx.IDrzava#remove(org.phoenix.app.modelx.Drzava)
	 */
	
	@Override
	public void remove(Drzava persistentInstance) {
		log.debug("removing Drzava instance");
		try {
			entityManager.remove(persistentInstance);
			log.debug("remove successful");
		} catch (RuntimeException re) {
			log.error("remove failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.modelx.IDrzava#merge(org.phoenix.app.modelx.Drzava)
	 */
	@Override
	public Drzava merge(Drzava detachedInstance) {
		log.debug("merging Drzava instance");
		try {
			Drzava result = entityManager.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	/* (non-Javadoc)
	 * @see org.phoenix.app.modelx.IDrzava#findById(java.lang.Integer)
	 */
	@Override
	public Drzava findById(Integer id) {
		log.debug("getting Drzava instance with id: " + id);
		try {
			Drzava instance = entityManager.find(Drzava.class, id);
			log.debug("get successful");
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	@Override
	public List<Drzava> findAll() {		
		TypedQuery<Drzava> query =  entityManager.createQuery("SELECT d FROM Drzava d",Drzava.class);
	     List<Drzava> lst = query.getResultList();
		return lst;
	}

	@Override
	public int removeById(int id) {
		log.debug("deleting Drzava instance with id: " + id);
		Query query = entityManager.createQuery("DELETE FROM Drzava c WHERE c.id = :id");
		return query.setParameter("id", id).executeUpdate();		
	}

	@Override
	public Drzava update(Drzava country) {
		log.debug("updating country: " + country);
		Drzava countryUpdated  = entityManager.merge(country);
		return countryUpdated ;
	}
}
