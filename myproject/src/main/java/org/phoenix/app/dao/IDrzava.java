package org.phoenix.app.dao;

import java.util.List;

import org.phoenix.app.model.Drzava;

public interface IDrzava {

	void persist(Drzava transientInstance);

	void remove(Drzava persistentInstance);
	
	int removeById(int id);

	Drzava merge(Drzava detachedInstance);

	Drzava findById(Integer id);

	List<Drzava> findAll();

	Drzava update(Drzava country);


}