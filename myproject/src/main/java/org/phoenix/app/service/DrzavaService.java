package org.phoenix.app.service;

import java.util.Date;
import java.util.List;

import org.phoenix.app.dao.IDrzava;
import org.phoenix.app.dto.CountryDTO;
import org.phoenix.app.model.Drzava;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Transactional
@Service("drzavaService")
public class DrzavaService {

	@Autowired
	IDrzava drzavaDao;

	@Transactional(readOnly = true)
	public List<Drzava> findAllCountries() {
		return drzavaDao.findAll();
	}

	public Drzava editCountry(CountryDTO countryDTO) {		
		Drzava country = countryDTO2Country(countryDTO);
		return drzavaDao.update(country);		
	}

	
	public void addCountry(CountryDTO countryDTO) {
		Drzava country = countryDTO2Country(countryDTO);
		drzavaDao.persist(country);
	}

	public int deleteCountry(int id) {		
		return drzavaDao.removeById(id);
	}

	private Drzava countryDTO2Country(CountryDTO countryDTO) {
		Drzava country = new Drzava();
		
		if(countryDTO.getId() != null){
			country.setId(countryDTO.getId());
		}
		
		country.setName(countryDTO.getName());
		country.setA2(countryDTO.getA2());
		country.setA3(countryDTO.getA3());
		country.setDisplayOrder(countryDTO.getDisplayOrder());
		country.setNumericCode(countryDTO.getNumericCode());
		country.setCreated(new Date());
		country.setCreatedBy("alaa");
		return country;
	}

}
