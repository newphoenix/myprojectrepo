Select id, name, display_order, created, created_by, modified, modified_by from car_db.fuel
USE car_db;

CREATE TABLE `fuel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `display_order` int(11) NOT NULL DEFAULT '50000',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;


INSERT INTO car_db.fuel(id,name,display_order,created,created_by,modified,modified_by) VALUES (1,'Diesel',50000,'2013-02-01 10:00:00','alaa','2014-10-29 13:24:09',null);
INSERT INTO car_db.fuel(id,name,display_order,created,created_by,modified,modified_by) VALUES (2,'Gasoline',50000,'2013-02-01 10:00:00','alaa','2014-10-29 13:24:09',null);
INSERT INTO car_db.fuel(id,name,display_order,created,created_by,modified,modified_by) VALUES (3,'Gas',50000,'2013-02-01 10:00:00','alaa','2014-10-29 13:24:09',null);
INSERT INTO car_db.fuel(id,name,display_order,created,created_by,modified,modified_by) VALUES (4,'Electric',50000,'2013-02-01 10:00:00','alaa','2014-10-29 13:24:09',null);
INSERT INTO car_db.fuel(id,name,display_order,created,created_by,modified,modified_by) VALUES (5,'Hybrid',50000,'2013-02-01 10:00:00','alaa','2014-10-29 13:24:09',null);
