Select id, username, user_code, name, surname, password, phone_number, birthday, gendar_id, country_id, shop, number_of_ads, picture, website, p_o_box, street, street_no, paid, paid_from, paid_to, static_position, last_time_log, enabled, created, created_by, modified, modified_by from car_db.user
USE car_db;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) NOT NULL,
  `user_code` varchar(32) NOT NULL DEFAULT 'a',
  `name` varchar(256) NOT NULL,
  `surname` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `phone_number` varchar(18) DEFAULT NULL,
  `birthday` date NOT NULL,
  `gendar_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `shop` tinyint(1) NOT NULL DEFAULT '0',
  `number_of_ads` int(11) NOT NULL DEFAULT '0',
  `picture` varchar(1024) DEFAULT ' ',
  `website` varchar(256) DEFAULT ' ',
  `p_o_box` varchar(64) DEFAULT ' ',
  `street` varchar(256) DEFAULT ' ',
  `street_no` varchar(16) DEFAULT ' ',
  `paid` tinyint(1) NOT NULL DEFAULT '0',
  `paid_from` timestamp NULL DEFAULT '2010-01-01 12:00:00',
  `paid_to` timestamp NULL DEFAULT '2010-01-01 12:00:00',
  `static_position` int(11) NOT NULL DEFAULT '11',
  `last_time_log` timestamp NULL DEFAULT '2001-01-01 10:00:00',
  `enabled` tinyint(1) NOT NULL DEFAULT '0',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(50) NOT NULL,
  `modified` timestamp NULL DEFAULT NULL,
  `modified_by` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `UK_ob8kqyqqgmefl0aco34akdtpe` (`username`),
  KEY `country_id` (`country_id`),
  KEY `gendar_id` (`gendar_id`),
  CONSTRAINT `country_fk8` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `gender_fk` FOREIGN KEY (`gendar_id`) REFERENCES `gender` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;


INSERT INTO car_db.user(id,username,user_code,name,surname,password,phone_number,birthday,enabled,created,created_by,modified,modified_by) VALUES (1,'alaa@gmail.com','18ihadqq6i1i17xuxe7uzyy7a','alaa','elabdullah','291312cd7bf6b6e1a75328944ac5b7371b4f0e9851971998e732078544c96b36f3f68b70fa9265f57878124e1a52ea4fc3d01cedf7caa332359d6f1d83567014','062/210-695','1980-02-18',1,'2012-05-23 14:00:00','admin',null,null);
INSERT INTO car_db.user(id,username,user_code,name,surname,password,phone_number,birthday,enabled,created,created_by,modified,modified_by) VALUES (2,'adnan@gmail.com','vna5ntyygy83ckeix1m7xb2z','adnan','mark','291312cd7bf6b6e1a75328944ac5b7371b4f0e9851971998e732078544c96b36f3f68b70fa9265f57878124e1a52ea4fc3d01cedf7caa332359d6f1d83567014','062148658','1985-12-12',1,'2012-05-23 14:05:00','admin',null,null);
INSERT INTO car_db.user(id,username,user_code,name,surname,password,phone_number,birthday,enabled,created,created_by,modified,modified_by) VALUES (3,'igor@yahoo.com','d64n60bqawfy15qnd2j47k4t5','igor','ivanov','291312cd7bf6b6e1a75328944ac5b7371b4f0e9851971998e732078544c96b36f3f68b70fa9265f57878124e1a52ea4fc3d01cedf7caa332359d6f1d83567014','061985625','1970-08-08',1,'2012-05-25 13:24:00','admin',null,null);
INSERT INTO car_db.user(id,username,user_code,name,surname,password,phone_number,birthday,enabled,created,created_by,modified,modified_by) VALUES (9,'delkey1001@gmail.com','1g4i24r8107km1ielwm33enqyh','car house Sarajevo d.o.o.',' ','291312cd7bf6b6e1a75328944ac5b7371b4f0e9851971998e732078544c96b36f3f68b70fa9265f57878124e1a52ea4fc3d01cedf7caa332359d6f1d83567014','','1950-01-01',1,'2013-03-10 15:19:28','admin',null,'delkey1001@gmail.com');
INSERT INTO car_db.user(id,username,user_code,name,surname,password,phone_number,birthday,enabled,created,created_by,modified,modified_by) VALUES (10,'alaa.elabdullah@gmail.com','1dgrct88u2bn015idhpex0wkfg','Auto kuća import export Sarajevo d.o.o.','ammx','608fc55de59030eb5f8bd4e68a250360790b1b9e27d6f95de3c60b0de65cb6703745dc34f33e75cf695f952208e2084b14a1306f67e49ebf4229f9b3b9ef1dee','061228337','1979-01-24',1,'2013-05-04 22:46:29','admin',null,'alaa@gmail.com');
INSERT INTO car_db.user(id,username,user_code,name,surname,password,phone_number,birthday,enabled,created,created_by,modified,modified_by) VALUES (11,'bpmfmjhn@sharklasers.com','lbkiy8j1xt83e8v569ybr84x','ams1','amx','291312cd7bf6b6e1a75328944ac5b7371b4f0e9851971998e732078544c96b36f3f68b70fa9265f57878124e1a52ea4fc3d01cedf7caa332359d6f1d83567014',null,'1950-01-01',1,'2013-05-04 23:12:08','admin',null,null);
INSERT INTO car_db.user(id,username,user_code,name,surname,password,phone_number,birthday,enabled,created,created_by,modified,modified_by) VALUES (12,'rehlcunq@sharklasers.com','1tc7xrg8jvizk12afl3rnk0nhj','ams2','amx','291312cd7bf6b6e1a75328944ac5b7371b4f0e9851971998e732078544c96b36f3f68b70fa9265f57878124e1a52ea4fc3d01cedf7caa332359d6f1d83567014',null,'1950-01-01',1,'2013-05-04 23:19:44','admin',null,null);
INSERT INTO car_db.user(id,username,user_code,name,surname,password,phone_number,birthday,enabled,created,created_by,modified,modified_by) VALUES (13,'pxndnibk@sharklasers.com','1x93h33krb4iz12uv90p3ew245','ams3','amx','291312cd7bf6b6e1a75328944ac5b7371b4f0e9851971998e732078544c96b36f3f68b70fa9265f57878124e1a52ea4fc3d01cedf7caa332359d6f1d83567014',null,'1950-01-01',1,'2013-05-04 23:22:33','admin',null,null);
INSERT INTO car_db.user(id,username,user_code,name,surname,password,phone_number,birthday,enabled,created,created_by,modified,modified_by) VALUES (14,'fjmqbjom@sharklasers.com','ce9w720ols2vf5kt51q1phm7','ams4','amx','291312cd7bf6b6e1a75328944ac5b7371b4f0e9851971998e732078544c96b36f3f68b70fa9265f57878124e1a52ea4fc3d01cedf7caa332359d6f1d83567014',null,'1950-01-01',1,'2013-05-04 23:24:18','admin',null,null);
INSERT INTO car_db.user(id,username,user_code,name,surname,password,phone_number,birthday,enabled,created,created_by,modified,modified_by) VALUES (15,'voices.application@gmail.com','1hadh4hc5dl2sU1j7210svu7cax','alaa','elabdullah','8045c18a5e4cdd827ceaa731ac87748815e2b0d3c33891ebb0f1ef609ec4194282b60acce05f1fbc12a3b53924315ee90c20e320ce94c65fbc8c3312448eb225',null,'1980-02-18',1,'2013-05-19 22:55:05','admin',null,null);
INSERT INTO car_db.user(id,username,user_code,name,surname,password,phone_number,birthday,enabled,created,created_by,modified,modified_by) VALUES (16,'delkey1001@yahoo.com','6u0ge3qch8b4Uo2dlv35yn9bg','yacu','mahi','7b11d28244139f0012b75567d55063f615e2b0d3c33891ebb0f1ef609ec41942b985c796878f408e2da86877178caacf0c20e320ce94c65fbc8c3312448eb225','063/228-995','1950-01-18',1,'2013-06-06 10:34:27','admin',null,'alaa@gmail.com');
INSERT INTO car_db.user(id,username,user_code,name,surname,password,phone_number,birthday,enabled,created,created_by,modified,modified_by) VALUES (19,'abc@gmail.com','abc','abc','efg','abc',null,'2015-08-18',0,'2015-08-10 00:00:00','admin',null,null);
