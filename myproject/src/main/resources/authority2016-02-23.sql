USE car_db;

CREATE TABLE `authority` (
  `username` varchar(60) NOT NULL,
  `authority` varchar(80) NOT NULL,
  PRIMARY KEY (`username`,`authority`),
  KEY `username` (`username`),
  CONSTRAINT `username_fk7` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO car_db.authority(username,authority) VALUES ('abc@gmail.com','ROLE_user');
INSERT INTO car_db.authority(username,authority) VALUES ('adnan@gmail.com','ROLE_manager');
INSERT INTO car_db.authority(username,authority) VALUES ('alaa.elabdullah@gmail.com','ROLE_user');
INSERT INTO car_db.authority(username,authority) VALUES ('alaa@gmail.com','ROLE_admin');
INSERT INTO car_db.authority(username,authority) VALUES ('alaa@gmail.com','ROLE_manager');
INSERT INTO car_db.authority(username,authority) VALUES ('bpmfmjhn@sharklasers.com','ROLE_user');
INSERT INTO car_db.authority(username,authority) VALUES ('delkey1001@gmail.com','ROLE_user');
INSERT INTO car_db.authority(username,authority) VALUES ('delkey1001@yahoo.com','ROLE_user');
INSERT INTO car_db.authority(username,authority) VALUES ('fjmqbjom@sharklasers.com','ROLE_user');
INSERT INTO car_db.authority(username,authority) VALUES ('pxndnibk@sharklasers.com','ROLE_user');
INSERT INTO car_db.authority(username,authority) VALUES ('rehlcunq@sharklasers.com','ROLE_user');
INSERT INTO car_db.authority(username,authority) VALUES ('voices.application@gmail.com','ROLE_user');
